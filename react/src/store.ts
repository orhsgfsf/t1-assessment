import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import logger from 'redux-logger';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk';

import { IItemsState } from './shoppingList/state'
import { IItemsAction } from './shoppingList/actions'
import { itemsReducer } from './shoppingList/reducers'

export const history = createBrowserHistory()

export interface IRootState {
    items: IItemsState
    router: RouterState
}

type IRootAction =
    IItemsAction |
    CallHistoryMethodAction

const rootReducer = combineReducers<IRootState>({
    items: itemsReducer,
    router: connectRouter(history)
})

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    ));