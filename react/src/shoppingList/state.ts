export interface IItem {
    id:number
    img:string
    name:string
    referenceCode:string
    quantity:number
    price:number
}

export interface IItemsState {
    items: IItem[]
}