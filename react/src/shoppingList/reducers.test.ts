import { IItemsState } from './state';
import { itemsReducer } from './reducers';
import { getItemsSuccess, changeQuantitySuccess, deleteItemSuccess } from './actions';

import momo from '../image/chicken-momo.jpg';
import potatoes from '../image/mexican-potatoes.jpg';
import breakfast from '../image/breakfast.jpg';

describe("UserDataEditReducer", () => {
    let initialState: IItemsState;

    beforeEach(() => {
        initialState = {
            items: []
        }
    });

    it("should get all items success", () => {
        const newState = itemsReducer(initialState, getItemsSuccess())
        expect(newState).toEqual({
            items:[
            {
                id: 0,
                img: momo,
                name: "Chicken momo",
                referenceCode: "#4231648",
                quantity: 3,
                price: 10.50
            },
            {
                id: 1,
                img: potatoes,
                name: "Spicy Mexican Potatoes",
                referenceCode: "#4231648",
                quantity: 2,
                price: 8.50
            },
            {
                id: 2,
                img: breakfast,
                name: "Breakfast",
                referenceCode: "#4231648",
                quantity: 1,
                price: 5.90
            }
        ]})
    });

    it("should change quantity success", () => {
        const newState = itemsReducer({
            items: [
                {
                    id: 0,
                    img: momo,
                    name: "Chicken momo",
                    referenceCode: "#4231648",
                    quantity: 3,
                    price: 10.50
                },
                {
                    id: 1,
                    img: potatoes,
                    name: "Spicy Mexican Potatoes",
                    referenceCode: "#4231648",
                    quantity: 2,
                    price: 8.50
                },
                {
                    id: 2,
                    img: breakfast,
                    name: "Breakfast",
                    referenceCode: "#4231648",
                    quantity: 1,
                    price: 5.90
                }
            ]
        }, changeQuantitySuccess(2, 3))
        expect(newState).toEqual({
            items: [
                {
                    id: 0,
                    img: momo,
                    name: "Chicken momo",
                    referenceCode: "#4231648",
                    quantity: 3,
                    price: 10.50
                },
                {
                    id: 1,
                    img: potatoes,
                    name: "Spicy Mexican Potatoes",
                    referenceCode: "#4231648",
                    quantity: 2,
                    price: 8.50
                },
                {
                    id: 2,
                    img: breakfast,
                    name: "Breakfast",
                    referenceCode: "#4231648",
                    quantity: 3,
                    price: 5.90
                }
            ]
        })
    });

    it('should delete item success', () => {
        const newState = itemsReducer({
            items: [
                {
                    id: 0,
                    img: momo,
                    name: "Chicken momo",
                    referenceCode: "#4231648",
                    quantity: 3,
                    price: 10.50
                },
                {
                    id: 1,
                    img: potatoes,
                    name: "Spicy Mexican Potatoes",
                    referenceCode: "#4231648",
                    quantity: 2,
                    price: 8.50
                },
                {
                    id: 2,
                    img: breakfast,
                    name: "Breakfast",
                    referenceCode: "#4231648",
                    quantity: 1,
                    price: 5.90
                }
            ]
        }, deleteItemSuccess(1))
        expect(newState).toEqual({
            items: [
                {
                    id: 0,
                    img: momo,
                    name: "Chicken momo",
                    referenceCode: "#4231648",
                    quantity: 3,
                    price: 10.50
                },
                {
                    id: 2,
                    img: breakfast,
                    name: "Breakfast",
                    referenceCode: "#4231648",
                    quantity: 1,
                    price: 5.90
                }
            ]
        })
    });
});