import momo from '../image/chicken-momo.jpg'
import potatoes from '../image/mexican-potatoes.jpg'
import breakfast from '../image/breakfast.jpg'

// this is to make the definition of different actions
export function getItemsSuccess() {
    return {
        type: 'GET_ITEMS_SUCCESS' as 'GET_ITEMS_SUCCESS',
        items: [
            {
                id: 0,
                img: momo,
                name: "Chicken momo",
                referenceCode: "#4231648",
                quantity: 3,
                price: 10.50
            },
            {
                id: 1,
                img: potatoes,
                name: "Spicy Mexican Potatoes",
                referenceCode: "#4231648",
                quantity: 2,
                price: 8.50
            },
            {
                id: 2,
                img: breakfast,
                name: "Breakfast",
                referenceCode: "#4231648",
                quantity: 1,
                price: 5.90
            }
        ]
    }
}

export function changeQuantitySuccess(id:number, quantity:number){
    return {
        type: 'CHANGE_QUANTITY_SUCCESS' as 'CHANGE_QUANTITY_SUCCESS',
        id,
        quantity
    }
}

export function deleteItemSuccess(id:number){
    return {
        type: 'DELETE_ITEM_SUCCESS' as 'DELETE_ITEM_SUCCESS',
        id
    }
}

// export the action
type itemsCreators = typeof getItemsSuccess | typeof  changeQuantitySuccess | typeof deleteItemSuccess
export type IItemsAction = ReturnType<itemsCreators>