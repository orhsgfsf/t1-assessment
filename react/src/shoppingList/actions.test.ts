import { getItemsSuccess, changeQuantitySuccess, deleteItemSuccess } from './actions';

import momo from '../image/chicken-momo.jpg';
import potatoes from '../image/mexican-potatoes.jpg';
import breakfast from '../image/breakfast.jpg';

describe('IItemsAction', () => {

    it("should return get all items action", () => {
        expect(getItemsSuccess()).toStrictEqual({
            type: 'GET_ITEMS_SUCCESS' as 'GET_ITEMS_SUCCESS',
            items: [
                {
                    id: 0,
                    img: momo,
                    name: "Chicken momo",
                    referenceCode: "#4231648",
                    quantity: 3,
                    price: 10.50
                },
                {
                    id: 1,
                    img: potatoes,
                    name: "Spicy Mexican Potatoes",
                    referenceCode: "#4231648",
                    quantity: 2,
                    price: 8.50
                },
                {
                    id: 2,
                    img: breakfast,
                    name: "Breakfast",
                    referenceCode: "#4231648",
                    quantity: 1,
                    price: 5.90
                }
            ]
        })
    });

    it('should return change quantity action', () => {
        expect(changeQuantitySuccess(1, 7)).toStrictEqual({
            type: 'CHANGE_QUANTITY_SUCCESS' as 'CHANGE_QUANTITY_SUCCESS',
            id: 1,
            quantity: 7
        })
    });

    it('should return delete item action', () => {
        expect(deleteItemSuccess(0)).toStrictEqual({
            type: 'DELETE_ITEM_SUCCESS' as 'DELETE_ITEM_SUCCESS',
            id: 0,  
        })
    });
});
