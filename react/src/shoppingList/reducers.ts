import { IItemsState } from './state'
import { IItemsAction } from './actions'

const initialState:IItemsState = {
    items: []
}

// base on different case form the action, they will do their jobs for what action they should do to make a proper new state. 
export function itemsReducer(state:IItemsState = initialState, action:IItemsAction): IItemsState {
    switch (action.type) {
        case 'GET_ITEMS_SUCCESS':
        return {
            ...state,
            items: action.items     
            }
        case 'CHANGE_QUANTITY_SUCCESS':
        return {
            ...state,
            items: state.items.map(item => {
                if (item.id !== action.id){
                    return item
                }else{
                    return {
                        ...item,
                        quantity:action.quantity
                    }
                }
            })
        }
        case 'DELETE_ITEM_SUCCESS':
        return {
            ...state,
            items: state.items.filter(item => item.id !== action.id)
        }
        default:
        return state
    }
}