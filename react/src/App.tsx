import React from 'react';
import { Provider } from 'react-redux';
import store, { history } from './store';
import { ConnectedRouter } from 'connected-react-router';

import { Container, Row, Col } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import ShoppingCart from './ShoppingCart';
import CreditCard from './CreditCard';

export default class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
            <div className="background">
              <Container>
                <div className="purchase-page">
                <Row>
                  <Col md="8">
                    <ShoppingCart/>
                  </Col>
                  <Col md="4">
                    <CreditCard />
                  </Col>
                </Row>
                </div>
              </Container>
            </div>
        </ConnectedRouter>
      </Provider>
    );
  }
};
