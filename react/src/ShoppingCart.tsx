import React from 'react';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from './store';
import { getItemsSuccess, changeQuantitySuccess, deleteItemSuccess } from './shoppingList/actions';
import { IItem } from './shoppingList/state';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Col, Row } from 'reactstrap';

// I use TypeScript so I have to define all the types of the props
interface IShoppingCartProps {
    items: IItem[];

    // those are function that finally will call dispatch functions so that the action will be dispatched in the redux store where is reducers
    getItems: () => void;
    changeQuantity: (id: number, quantity: number) => void;
    deleteItem: (id: number) => void;
};

class ShoppingCart extends React.PureComponent<IShoppingCartProps> {

    // to add quantity of the item
    private add = (id:number, quantity:number) => {
        quantity++
        this.props.changeQuantity(id, quantity)
    };

    // to subtract quantity of the item
    private subtract = (id:number, quantity:number) => {
        if (quantity > 0) {
            quantity--
            this.props.changeQuantity(id, quantity)
        }
    };

    // to delete item
    private deleteItem = (id:number) => {
        this.props.deleteItem(id)
    };

    // to do the life cycle of the react
    public componentDidMount() {
        this.props.getItems()
    };

    //render 
    public render() {
        return (
            <div className="shopping-cart">
                <Row>
                    <Col>
                        <div className="title"><b>Shopping Cart</b></div>
                    </Col>
                </Row>
                {this.props.items.map(item =>
                    <div className="item">
                        <Row>
                            <Col md="2">
                                <img className="food-image" src={item.img} alt=""/>

                            </Col>
                            <Col md="4" className="food-name">
                                <div>
                                    <div className="item-name"><b>{item.name}</b></div>
                                    <div className="reference-code">{item.referenceCode}</div>
                                </div>
                            </Col>
                            <Col md="2" className="qty">
                                <button className="button" onClick={() => this.add(item.id, item.quantity)}><b>+</b></button>
                                <div className="qty-block">{item.quantity}</div>
                                <button className="button" onClick={() => this.subtract(item.id, item.quantity)}><b>-</b></button>
                            </Col>
                            <Col md="3" className="price-and-symbol">
                                <div>
                                    <b className="currency-symbol">$</b> <b>{(item.quantity * item.price).toFixed(2)}</b>
                                </div>
                            </Col>
                            <Col md="1" className="price-and-delete-button">
                                <button className="button delete-button" onClick={() => this.deleteItem(item.id)}>{<FontAwesomeIcon icon={faTimes} size="1x" />}</button>
                            </Col>
                        </Row>
                    </div>
                )}
                <div className="bottom-list">
                    <div className="continue-shopping">
                        <div className="arrow">
                        {<FontAwesomeIcon icon={faArrowLeft} size="1x" color="grey" />}&nbsp;<a href="/"><b>continue shopping</b></a>
                        </div>
                    </div>
                    <div className="sub-total">
                        <div>
                            <b className="subtotal-size">Subtotal:</b>&nbsp;
                            <b className="price-size">
                                {`$${(this.props.items.reduce((prev, next) => {
                                    return prev += next.quantity * next.price
                                }, 0)).toFixed(2)}`}
                            </b>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

// this is to make get the new state that has been updated by redux action and reducers from the old state to react and re-render will be done
const mapStateToProps = (state:IRootState) => ({
    items: state.items.items
});

// this is to dispatch the action to redux store, some of them will go to redux thunk first
const mapDispatchToProps = (dispatch:ThunkDispatch) => ({
    getItems: () => dispatch(getItemsSuccess()),
    changeQuantity: (id:number, quantity:number) => dispatch(changeQuantitySuccess(id, quantity)),
    deleteItem: (id:number) => dispatch(deleteItemSuccess(id))
});

// connect the react and redux
export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
