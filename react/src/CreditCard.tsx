import React from 'react';
import './css/creditCard.css'
import Cards from 'react-credit-cards';
import { CallbackArgument } from 'react-credit-cards'
import { Col, Row, Button } from 'reactstrap';
import visa from './image/visa.png'
import mastercard from './image/mastercard.png'

// I use TypeScript so I have to define all the types of the props.
interface ICreditCardState {
    name: string
    number: string
    expiry: string | number
    focused: any
    cvc: string | number

    selectedCardType: boolean
    correctCardNum: boolean
    errMsgOfMaxLength: string | null
    errorType: string | null

    cardType: string
    cardTypeChangedByNumber: string
};

export default class CreditCard extends React.PureComponent<{}, ICreditCardState>{
    constructor(props: {}) {
        super(props);

        // set the initial state
        this.state = {
            name: '',
            number: '',
            expiry: '',
            focused: '',
            cvc: '',

            selectedCardType: true,
            correctCardNum: true,
            errMsgOfMaxLength: null,
            errorType: null,

            cardType: "",
            cardTypeChangedByNumber: "unknown"
        };
    };

    // focus on the target of the credit card
    private handleInputFocus = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({ focused: e.currentTarget.name });
    };

    // this is to change the value when I type one character or delete one character in the input row
    // when I type in different rows such as name, the field will be automatically change to name and the state of the name will also be changed depends on the event.currentTarget.value
    // when I type in cvc row, the field will be changed to cvc and the state of the cvc will also ba changed depends on the event.currentTarget.value
    private handleInputChange = (field: "name" | "number" | "expiry" | "cvc", e: React.FormEvent<HTMLInputElement>) => {
        const state: any = {}
        if (this.state.cardType !== "") {
            state[field] = e.currentTarget.value
            this.setState(state)
        } else {
            state[field] = ""
            this.setState(state)
            this.setState({ selectedCardType: false })
        }
    };

    // this is to change the card type by clicking the image of the card type
    private cardTypeChange = (cardType: string) => {
        this.setState({ cardType })
        this.setState({ selectedCardType: true })
    };

    // render
    public render() {
        return (
            <div className="credit-card-background">
                <b>Card Details</b>
                <div id="CreditCard">
                    <div className="card-type">
                        <b className="card-title">{this.state.cardType === "" ? "Please select Card Type:" : "Card Type:"}</b>
                        {this.state.cardType === "" &&
                            <div className="select-card-type">
                                {<img src={visa} width="100px" onClick={() => this.cardTypeChange('visa')} alt="" />}
                                {<img src={mastercard} width="80px" onClick={() => this.cardTypeChange('mastercard')} alt="" />}
                            </div>
                        }
                        <div className="select-card-message">
                            {this.state.selectedCardType === false && <div className="wrong-card-message"><b>* Please select the Card Type first.</b></div>}
                        </div>
                        {
                            <div className={this.state.cardType !== "" ? "credit-card-details" : ""}>
                                {this.state.cardType !== "" &&
                                    <Cards
                                        cvc={this.state.cvc}
                                        expiry={this.state.expiry}
                                        focused={this.state.focused}
                                        name={this.state.name}
                                        number={this.state.number}
                                        preview={true}
                                        issuer={this.state.cardType}
                                        acceptedCards={["visa", "mastercard"]}
                                        locale={{ valid: '' }}
                                        placeholders={{ name: "YOUR NAME" }}
                                        callback={(type: CallbackArgument, isValid: boolean) => {
                                            if (!isValid && this.state.correctCardNum) {
                                                this.setState({
                                                    correctCardNum: false
                                                })
                                            } else if (isValid && !this.state.correctCardNum) {
                                                this.setState({
                                                    correctCardNum: true
                                                })
                                            }
                                            if (this.state.number.length > type.maxLength) {
                                                this.setState({
                                                    errMsgOfMaxLength: `The length of the Card Number cannot exceed ${type.maxLength} numbers.`
                                                })
                                            } else {
                                                this.setState({
                                                    errMsgOfMaxLength: null
                                                })
                                            }
                                            if (this.state.cardType !== type.issuer) {
                                                this.setState({
                                                    errorType: this.state.cardType
                                                })
                                            } else {
                                                this.setState({
                                                    errorType: null
                                                })
                                            }
                                            this.setState({
                                                cardTypeChangedByNumber: type.issuer
                                            })
                                        }}
                                    />
                                }
                                <div className="change-card-type">
                                    {this.state.cardType === "mastercard" && <img src={visa} width="80px" onClick={() => this.cardTypeChange('visa')} alt="" />}
                                    {this.state.cardType === "visa" && <img src={mastercard} width="80px" onClick={() => this.cardTypeChange('mastercard')} alt="" />}
                                </div>
                            </div>
                        }
                    </div>
                    <div className="credit-card-detail">
                        <form>
                            <div className="card-input">
                                <b className="card-title">Name on Card:</b>
                                <input
                                    type="text"
                                    name="name"
                                    className="form-control"
                                    placeholder="Name"
                                    required
                                    onChange={this.handleInputChange.bind(this, "name")}
                                    onFocus={this.handleInputFocus}
                                    value={this.state.name}
                                />
                            </div>
                            <div className="card-input">
                                <b className="card-title">Card Number:</b>
                                <input
                                    type="tel"
                                    name="number"
                                    className="form-control"
                                    placeholder="Card Number"
                                    onChange={this.handleInputChange.bind(this, "number")}
                                    onFocus={this.handleInputFocus}
                                    value={this.state.number}
                                />
                                {this.state.number.length >= 4 && this.state.cardType !== this.state.cardTypeChangedByNumber && <div className="wrong-card-message"><b>* This Card number is NOT {this.state.cardType.substring(0, 1).toUpperCase() + this.state.cardType.substring(1).toLowerCase()} number.</b></div>}
                                {this.state.number.length !== 0 && !this.state.correctCardNum && <div><b className="wrong-card-message">* This Card Number is NOT correct.</b></div>}
                                {this.state.cardTypeChangedByNumber !== "unknown" && this.state.errMsgOfMaxLength && <div><b className="wrong-card-message">* {this.state.errMsgOfMaxLength}</b></div>}
                            </div>
                            <Row>
                                <Col md="6">
                                    <div className="card-input">
                                        <b className="card-title">Expiration Date:</b>
                                        <input
                                            type="tel"
                                            name="expiry"
                                            className="form-control"
                                            placeholder="MM/YY"
                                            pattern="\d\d/\d\d"
                                            required
                                            onChange={this.handleInputChange.bind(this, "expiry")}
                                            onFocus={this.handleInputFocus}
                                            value={this.state.expiry}
                                        />
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="card-input">
                                        <b className="card-title">CVC/CVV:</b>
                                        <input
                                            type="tel"
                                            name="cvc"
                                            className="form-control"
                                            placeholder="CVC/CVV"
                                            pattern="\d{3,4}"
                                            required
                                            onChange={this.handleInputChange.bind(this, "cvc")}
                                            onFocus={this.handleInputFocus}
                                            value={this.state.cvc}
                                        />
                                        {typeof this.state.cvc === "string" && this.state.cvc !== "" && this.state.cvc.match(/^[0-9]+(\.[0-9]+)?$/) === null && <div><b className="wrong-card-message">* CVV/CVC can only be numbers</b></div>}
                                        {typeof this.state.cvc === "string" && this.state.cvc.length > 3 && <div><b className="wrong-card-message">* CVV/CVC cannot exceed 3 numbers</b></div>}
                                    </div>
                                </Col>
                            </Row>
                        </form>
                        <Row>
                            <Col md="12">
                                <div className="check-out-button">
                                    <Button color="primary" size="lg" block><b className="check-out-size">Check Out</b></Button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
};
